# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* To design and implement Springboot Rest API's with CRUD operations using a cache service and Transaction support using the TDD approach
* 0.0.1

### How do I get set up? ###

* Technologies used 
* Java 8
* Spring boot
* EHCache
* MySQL
* Junit

### Ehcache ###

* Refer resources\ehcache.xml file for the configuration details on max entries, timeout of entries, ttl for an entry
* Refer the class files for the annotations related to the Ehcaching concept
* CacheEvict is also implemented at the method level

### Other features ###

* Transaction support using AOP - Refer LoggingAspect file under the package com.springboot.aop
* Unit test cases were written for the Controller and Service layer class files
* 16 test cases were written for this particular project

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact