package com.springboot.dao;

import com.springboot.entities.AppTransactionLogger;

import java.sql.Connection;

public interface RecordActivityDao {
    public Connection connect();

    public void save(AppTransactionLogger tAppActivityLog);

}