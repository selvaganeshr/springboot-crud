package com.springboot.service;

import java.util.ArrayList;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import com.springboot.bo.ParentTaskVO;
import com.springboot.bo.ProjectVO;
import com.springboot.bo.TaskVO;
import com.springboot.bo.UserVO;
import com.springboot.entity.ParentTask;
import com.springboot.entity.Project;
import com.springboot.entity.Task;
import com.springboot.entity.User;
import com.springboot.repository.ParentTaskManagerRepository;
import com.springboot.repository.ProjectManagerRepository;
import com.springboot.repository.TaskManagerRepository;
import com.springboot.repository.UserManagerRepository;
import com.springboot.service.ProjectManagerService;

@Service
public class ProjectManagerServiceImpl implements ProjectManagerService{

	private static final Logger LOGGER = LoggerFactory.getLogger(ProjectManagerServiceImpl.class);

	private ProjectManagerRepository projectManagerRepository;
	
	private TaskManagerRepository taskManagerRepository;
	
	private ParentTaskManagerRepository parentTaskManagerRepository;
	
	private UserManagerRepository userManagerRepository;
	
	private Mapper dozerMapper;

	@Autowired
	private CacheManager cacheManager;
	
	@Autowired
	public ProjectManagerServiceImpl(ProjectManagerRepository projectManagerRepository,
			TaskManagerRepository taskManagerRepository, 
			ParentTaskManagerRepository parentTaskManagerRepository,
			UserManagerRepository userManagerRepository, Mapper dozerMapper) {
		this.projectManagerRepository = projectManagerRepository;
		this.taskManagerRepository = taskManagerRepository;
		this.parentTaskManagerRepository = parentTaskManagerRepository;
		this.userManagerRepository = userManagerRepository;
		this.dozerMapper = dozerMapper;
	}

	@CacheEvict(value="tasksInfoCache", allEntries=true)
	public List<TaskVO> retriveTasks(){
		List<TaskVO> taskToBeReturned = new ArrayList<TaskVO>();
		List<Task> tasksRetrived = taskManagerRepository.findAll();
		Cache cache = cacheManager.getCache("employeeInfoCache");
		Element cacheElementAsEmployeeId = cache.get(taskToBeReturned);
		if (null == cacheElementAsEmployeeId) {
			cache.put(new Element(cacheElementAsEmployeeId, taskToBeReturned));
			for(Task task: tasksRetrived) {
				taskToBeReturned.add(dozerMapper.map(task, TaskVO.class));
			}
			return taskToBeReturned;
		} else {
			TaskVO cachedEmployeeData = (TaskVO) cacheElementAsEmployeeId.getObjectValue();
			taskToBeReturned.add(cachedEmployeeData);
			LOGGER.info(" Reading From EHCache" + cachedEmployeeData);
			return taskToBeReturned;
		}

	}
	
	public void updateTask(TaskVO task) {
		taskManagerRepository.save(dozerMapper.map(task, Task.class));
	}
	
	
	public List<ParentTaskVO> retriveParentTasks(){
		List<ParentTaskVO> taskToBeReturned = new ArrayList<ParentTaskVO>();
		List<ParentTask> tasksRetrived = parentTaskManagerRepository.findAll();
		for(ParentTask task: tasksRetrived) {
			taskToBeReturned.add(dozerMapper.map(task, ParentTaskVO.class));
		}
		return taskToBeReturned;
	}
	
	public List<ParentTaskVO> retriveParentTasksForProjectId(String projectId){
		List<ParentTaskVO> taskToBeReturned = new ArrayList<ParentTaskVO>();
		List<ParentTask> tasksRetrived = parentTaskManagerRepository.findAllParentTaskByProjectId(projectId); 
		for(ParentTask task: tasksRetrived) {
			taskToBeReturned.add(dozerMapper.map(task, ParentTaskVO.class));
		}
		return taskToBeReturned;
	}
	
	public void updateParentTask(ParentTaskVO parentTask) {
		parentTaskManagerRepository.save(dozerMapper.map(parentTask, ParentTask.class));
	}
	
	
	public List<ProjectVO> retriveProjects(){
		List<ProjectVO> projectToBeReturned = new ArrayList<ProjectVO>();
		List<Project> projectsRetrived = projectManagerRepository.findAll();
		for(Project project: projectsRetrived) {
			ProjectVO projectVO = dozerMapper.map(project, ProjectVO.class);
			projectVO.setNoOfTasks(taskManagerRepository.getTotalTasksForProjectId(projectVO.getProjectId()));
			projectToBeReturned.add(projectVO);
		}
		return projectToBeReturned;
	}
	
	public void updateProject(ProjectVO project) {
		projectManagerRepository.save(dozerMapper.map(project, Project.class));
	}

	@CacheEvict(value="userInfoCache", allEntries=true)
	public List<UserVO> retriveUsers(){
		List<UserVO> userToBeReturned = new ArrayList<UserVO>();
		List<User> usersRetrived = userManagerRepository.findAll();
		Cache cache = cacheManager.getCache("employeeInfoCache");
		Element cacheElementAsEmployeeId = cache.get(userToBeReturned);
		if (null == cacheElementAsEmployeeId) {
			cache.put(new Element(cacheElementAsEmployeeId, userToBeReturned));
			for(User user: usersRetrived) {
				userToBeReturned.add(dozerMapper.map(user, UserVO.class));
			}
			return userToBeReturned;
		} else {
			UserVO cachedEmployeeData = (UserVO) cacheElementAsEmployeeId.getObjectValue();
			userToBeReturned.add(cachedEmployeeData);
			LOGGER.info(" Reading From EHCache" + cachedEmployeeData);
			return userToBeReturned;
		}

	}
	
	public void updateUser(UserVO user) {
		User userStore = dozerMapper.map(user, User.class);
		userManagerRepository.save(userStore);
	}
	
}
