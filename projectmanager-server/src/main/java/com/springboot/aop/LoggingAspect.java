package com.springboot.aop;

import com.springboot.dao.RecordActivityDao;
import com.springboot.entities.AppTransactionLogger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.util.Date;

@Aspect
@Component
public class LoggingAspect
{
    @Autowired
    private RecordActivityDao recordActivityDao;
    long i=0;

    @Around("execution(* com.springboot.controller..*(..)))")
    public Object profileAllMethods(ProceedingJoinPoint proceedingJoinPoint) throws Throwable
    {
        MethodSignature methodSignature = (MethodSignature) proceedingJoinPoint.getSignature();

        String className = methodSignature.getDeclaringType().getSimpleName();
        String methodName = methodSignature.getName();

        final StopWatch stopWatch = new StopWatch();

        stopWatch.start();
        Object result = proceedingJoinPoint.proceed();
        stopWatch.stop();

        AppTransactionLogger appActivityLogger = new AppTransactionLogger(i++, methodName, stopWatch.getTotalTimeMillis(), new Date());
        System.out.println(appActivityLogger.toString());
        recordActivityDao.save(appActivityLogger);

        return result;
    }
}